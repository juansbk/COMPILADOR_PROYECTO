%option yylineno
%{

#include <ctype.h>
#include <stdio.h>
#include "sintactico.tab.h"

int lineno;
static int yywrap(void);
static void omitir_comentario(void);
static int identificador_check(const char *);
%}

sufijo_int				([uU][lL]?)|([lL][uU]?)
fracciones_const		([0-9]*\.[0-9]+)|([0-9]+\.)
exppart					[eE][-+]?[0-9]+
sufijo_float			[fFlL]
texto_char				([^'])|(\\.)
texto_string			([^"])|(\\.)

%%
"\n"					{ ++lineno; }
[\t\f\v\r ]+			{ /* Ignorar espacios en blanco. */ }
"/*"					{ omitir_comentario(); }
"{"						{ return '{'; }
"<%"					{ return '{'; }
"}"						{ return '}'; }
"%>"					{ return '}'; }
"["						{ return '['; }
"<:"					{ return '['; }
"]"						{ return ']'; }
":>"					{ return ']'; }
"("						{ return '('; }
")"						{ return ')'; }
";"						{ return ';'; }
":"						{ return ':'; }
"..."					{ return ELLIPSIS; }
"?"						{ return '?'; }
"."						{ return '.'; }
"+"						{ return '+'; }
"-"						{ return '-'; }
"*"						{ return '*'; }
"/"						{ return '/'; }
"%"						{ return '%'; }
"^"						{ return '^'; }
"&"						{ return '&'; }
"|"						{ return '|'; }
"~"						{ return '~'; }
"!"						{ return '!'; }
"="						{ return '='; }
"<"						{ return '<'; }
">"						{ return '>'; }
"+="					{ return MAS_IGUAL; }
"-="					{ return MENOS_IGUAL; }
"*="					{ return MUL_IGUAL; }
"/="					{ return DIV_IGUAL; }
"%="					{ return MOD_IGUAL; }
"^="					{ return XOR_IGUAL; }
"&="					{ return AND_IGUAL; }
"|="					{ return OR_IGUAL; }
"<<"					{ return DESP_IZQ; }
">>"					{ return DESP_DER; }
"<<="					{ return DESP_IZQ_IGUAL; }
">>="					{ return DESP_DER_IGUAL; }
"=="					{ return IGUAL; }
"!="					{ return NO_IGUAL; }
"<="					{ return MENOR_IGUAL; }
">="					{ return MAYOR_IGUAL; }
"&&"					{ return ANDAND; }
"||"					{ return OROR; }
"++"					{ return MASMAS; }
"--"					{ return MENOSMENOS; }
","						{ return ','; }
"->"					{ return FLECHA; }

"auto"					{ return AUTO; }
"break"					{ return BREAK; }
"case"					{ return CASE; }
"char"					{ return CHAR; }
"const"					{ return CONST; }
"continue"				{ return CONTINUE; }
"default"				{ return DEFAULT; }
"do"					{ return DO; }
"double"				{ return DOUBLE; }
"else"					{ return ELSE; }
"enum"					{ return ENUM; }
"extern"				{ return EXTERN; }
"float"					{ return FLOAT; }
"for"					{ return FOR; }
"goto"					{ return GOTO; }
"if"					{ return IF; }
"int"					{ return INT; }
"long"					{ return LONG; }
"register"				{ return REGISTER; }
"return"				{ return RETURN; }
"short"					{ return SHORT; }
"signed"				{ return SIGNED; }
"sizeof"				{ return SIZEOF; }
"static"				{ return STATIC; }
"struct"				{ return STRUCT; }
"switch"				{ return SWITCH; }
"typedef"				{ return TYPEDEF; }
"union"					{ return UNION; }
"unsigned"				{ return UNSIGNED; }
"void"					{ return VOID; }
"volatile"				{ return VOLATILE; }
"while"					{ return WHILE; }


[a-zA-Z_][a-zA-Z_0-9]*			{ return identificador_check(yytext); }

"0"[xX][0-9a-fA-F]+{sufijo_int}?		{ return ENTERO; }
"0"[0-7]+{sufijo_int}?			{ return ENTERO; }
[0-9]+{sufijo_int}?			{ return ENTERO; }

{fracciones_const}{exppart}?{sufijo_float}?	{ return FLOTANTE; }
[0-9]+{exppart}{sufijo_float}?		{ return FLOTANTE; }

"'"{texto_char}*"'"			{ return CARACTER; }
"L'"{texto_char}*"'"	 		{ return CARACTER; }

"\""{texto_string}*"\""			{ return CADENA; }
"L\""{texto_string}*"\""			{return CADENA; }

.					{ fprintf(stderr, "%d: caracter inseperado `%c'\n", lineno, yytext[0]); }

%%

static int
yywrap(void)
{
	return 1;
}

/*
 * Usamos esta rutina en vez del modelo lexicográfico porque no necesitamos guardar este comentario en 
 * buffer yytext
 */
static void
omitir_comentario(void)
{
	int c1, c2;

	c1 = input();
	c2 = input();

	while (c2 != EOF && !(c1 == '*' && c2 == '/')) {
		if (c1 == '\n')
			++lineno;
		c1 = c2;
		c2 = input();
	}
}

static int
identificador_check(const char *s)
{
	/*
	 * Esta funcion revisaría si `s' es un tipo nombre o un identificador
	 */

	if (isupper(s[0]))
		return TYPEDEF_NOMBRE;

	return IDENTIFICADOR;
}