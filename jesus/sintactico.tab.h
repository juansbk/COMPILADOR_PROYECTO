
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     IDENTIFICADOR = 258,
     TYPEDEF_NOMBRE = 259,
     ENTERO = 260,
     FLOTANTE = 261,
     CARACTER = 262,
     CADENA = 263,
     ELLIPSIS = 264,
     MAS_IGUAL = 265,
     MENOS_IGUAL = 266,
     MUL_IGUAL = 267,
     DIV_IGUAL = 268,
     MOD_IGUAL = 269,
     XOR_IGUAL = 270,
     AND_IGUAL = 271,
     OR_IGUAL = 272,
     DESP_IZQ = 273,
     DESP_DER = 274,
     DESP_IZQ_IGUAL = 275,
     DESP_DER_IGUAL = 276,
     IGUAL = 277,
     NO_IGUAL = 278,
     MENOR_IGUAL = 279,
     MAYOR_IGUAL = 280,
     ANDAND = 281,
     OROR = 282,
     MASMAS = 283,
     MENOSMENOS = 284,
     FLECHA = 285,
     AUTO = 286,
     BREAK = 287,
     CASE = 288,
     CHAR = 289,
     CONST = 290,
     CONTINUE = 291,
     DEFAULT = 292,
     DO = 293,
     DOUBLE = 294,
     ELSE = 295,
     ENUM = 296,
     EXTERN = 297,
     FLOAT = 298,
     FOR = 299,
     GOTO = 300,
     IF = 301,
     INT = 302,
     LONG = 303,
     REGISTER = 304,
     RETURN = 305,
     SHORT = 306,
     SIGNED = 307,
     SIZEOF = 308,
     STATIC = 309,
     STRUCT = 310,
     SWITCH = 311,
     TYPEDEF = 312,
     UNION = 313,
     UNSIGNED = 314,
     VOID = 315,
     VOLATILE = 316,
     WHILE = 317
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


