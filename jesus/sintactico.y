
%{
	#include <stdio.h>
	#include "lex.yy.c"
	extern int lineno;
	static void yyerror(const char *s);
%}

%token IDENTIFICADOR TYPEDEF_NOMBRE ENTERO FLOTANTE CARACTER CADENA
%token ELLIPSIS MAS_IGUAL MENOS_IGUAL MUL_IGUAL DIV_IGUAL MOD_IGUAL XOR_IGUAL AND_IGUAL OR_IGUAL DESP_IZQ DESP_DER 
%token DESP_IZQ_IGUAL DESP_DER_IGUAL IGUAL NO_IGUAL MENOR_IGUAL MAYOR_IGUAL ANDAND OROR MASMAS MENOSMENOS FLECHA
%token AUTO BREAK CASE CHAR CONST CONTINUE DEFAULT DO DOUBLE ELSE ENUM
%token EXTERN FLOAT FOR GOTO IF INT LONG REGISTER RETURN SHORT SIGNED SIZEOF
%token STATIC STRUCT SWITCH TYPEDEF UNION UNSIGNED VOID VOLATILE WHILE

%start traduccion_unit

%%
/* Expresiones. */
expresion_primaria:
	identificador
	| ENTERO
	| CARACTER
	| FLOTANTE
	| CADENA
	| '(' expresion')'
	;

identificador:
	IDENTIFICADOR {printf("Sentencia Correcta:%d\n",lineno);}
	;

expresion_postfija:
	expresion_primaria
	| expresion_postfija '[' expresion']'
	| expresion_postfija '(' lista_expresiones_argumento ')'
	| expresion_postfija '(' ')'
	| expresion_postfija '.' identificador
	| expresion_postfija FLECHA identificador
	| expresion_postfija MASMAS
	| expresion_postfija MENOSMENOS
	;

lista_expresiones_argumento:
	expresion_asignacion
	| lista_expresiones_argumento ',' expresion_asignacion
	;

expresion_unaria:
	expresion_postfija
	| MASMAS expresion_unaria
	| MENOSMENOS expresion_unaria
	| operador_unario emitir_expresion
	| SIZEOF expresion_unaria
	| SIZEOF '(' nombre_tipo ')'
	;

operador_unario:
	'&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

emitir_expresion:
	expresion_unaria
	| '(' nombre_tipo ')' emitir_expresion
	;

expresion_multiplicativa:
	emitir_expresion
	| expresion_multiplicativa '*' emitir_expresion
	| expresion_multiplicativa '/' emitir_expresion
	| expresion_multiplicativa '%' emitir_expresion
	;

expresion_aditiva:
	expresion_multiplicativa
	| expresion_aditiva '+' expresion_multiplicativa
	| expresion_aditiva '-' expresion_multiplicativa
	;

expresion_cambio:
	expresion_aditiva
	| expresion_cambio DESP_IZQ expresion_aditiva
	| expresion_cambio DESP_DER expresion_aditiva
	;

expresion_relacional:
	expresion_cambio
	| expresion_relacional '<' expresion_cambio
	| expresion_relacional '>' expresion_cambio
	| expresion_relacional MENOR_IGUAL expresion_cambio
	| expresion_relacional MAYOR_IGUAL expresion_cambio
	;

expresion_igualdad:
	expresion_relacional
	| expresion_igualdad IGUAL expresion_relacional
	| expresion_igualdad NO_IGUAL expresion_relacional
	;

expresion_and:
	expresion_igualdad
	| expresion_and '&' expresion_igualdad
	;

expresion_or_exclusivo:
	expresion_and
	| expresion_or_exclusivo '^' expresion_and
	;

expresion_or_inclusivo:
	expresion_or_exclusivo
	| expresion_or_inclusivo '|' expresion_or_exclusivo
	;

expresion_logica_and:
	expresion_or_inclusivo
	| expresion_logica_and ANDAND expresion_or_inclusivo
	;

expresion_logica_or:
	expresion_logica_and
	| expresion_logica_or OROR expresion_logica_and
	;

expresion_condicional:
	expresion_logica_or
	| expresion_logica_or '?' expresion':' expresion_condicional
	;

expresion_asignacion:
	expresion_condicional
	| expresion_unaria operador_asignacion expresion_asignacion 
	;

operador_asignacion:
	'='
	| MUL_IGUAL
	| DIV_IGUAL
	| MOD_IGUAL
	| MAS_IGUAL
	| MENOS_IGUAL
	| DESP_IZQ_IGUAL
	| DESP_DER_IGUAL
	| AND_IGUAL
	| XOR_IGUAL
	| OR_IGUAL
	;

expresion:
	expresion_asignacion
	| expresion',' expresion_asignacion
	;

expresion_constante:
	expresion_condicional
	;

declaracion:
	especificadores_declaracion lista_declarador_inic ';'
	| especificadores_declaracion ';'
	;

especificadores_declaracion:
	especificador_clase_almacenamiento especificadores_declaracion
	| especificador_clase_almacenamiento
	| tipo_especificador especificadores_declaracion
	| tipo_especificador
	| tipo_calificador especificadores_declaracion
	| tipo_calificador
	;

lista_declarador_inic:
	declarador_inic
	| lista_declarador_inic ',' declarador_inic
	;

declarador_inic:
	declarador
	| declarador '=' inicializador
	;

especificador_clase_almacenamiento:
	TYPEDEF
	| EXTERN
	| STATIC
	| AUTO
	| REGISTER
	;

tipo_especificador:
	VOID
	| CHAR
	| SHORT
	| INT
	| LONG
	| FLOAT
	| DOUBLE
	| SIGNED
	| UNSIGNED
	| especificador_estruct_union_or
	| especificador_enum
	| TYPEDEF_NOMBRE
	;

especificador_estruct_union_or:
	estruct_union_or identificador '{' lista_declaracion_estruct '}'
	| estruct_union_or '{' lista_declaracion_estruct '}'
	| estruct_union_or identificador
	;

estruct_union_or:
	STRUCT
	| UNION
	;

lista_declaracion_estruct:
	declaracion_estruct
	| lista_declaracion_estruct declaracion_estruct
	;

declaracion_estruct:
	lista_calific_especific lista_declarador_struct ';'
	;

lista_calific_especific:
	tipo_especificador lista_calific_especific
	| tipo_especificador
	| tipo_calificador lista_calific_especific
	| tipo_calificador
	;

lista_declarador_struct:
	declarador_estruct
	| lista_declarador_struct ',' declarador_estruct
	;

declarador_estruct:
	declarador
	|  ':' expresion_constante
	| declarador ':' expresion_constante
	;

especificador_enum:
	ENUM identificador '{' lista_enumerador '}'
	| ENUM '{' lista_enumerador '}'
	| ENUM identificador
	;

lista_enumerador:
	enumerador
	| lista_enumerador ',' enumerador
	;

enumerador:
	identificador
	| identificador '=' expresion_constante
	;

tipo_calificador:
	CONST
	| VOLATILE
	;

declarador:
	puntero declarador_directo
	| declarador_directo
	;

declarador_directo:
	identificador
	| '(' declarador ')'
	| declarador_directo '[' expresion_constante ']'
	| declarador_directo '[' ']'
	| declarador_directo '(' lista_tipo_parametro ')'
	| declarador_directo '(' lista_identificador ')'
	| declarador_directo '(' ')'
	;

puntero:
	'*' lista_tipo_calificador
	| '*'
	| '*' lista_tipo_calificador puntero
	| '*' puntero
	;

lista_tipo_calificador:
	tipo_calificador
	| lista_tipo_calificador tipo_calificador
	;

lista_tipo_parametro:
	lista_parametro
	| lista_parametro ',' ELLIPSIS
	;

lista_parametro:
	declaracion_parametro
	| lista_parametro ',' declaracion_parametro
	;

declaracion_parametro:
	especificadores_declaracion declarador
	| especificadores_declaracion declarador_abstracto
	| especificadores_declaracion
	;

lista_identificador:
	identificador
	| lista_identificador ',' identificador
	;

nombre_tipo:
	lista_calific_especific
	| lista_calific_especific declarador_abstracto
	;

declarador_abstracto:
	puntero
	| declarador_abstracto_directo
	| puntero declarador_abstracto_directo
	;

declarador_abstracto_directo:
	'(' declarador_abstracto ')'
	| '[' ']'
	| '[' expresion_constante ']'
	| declarador_abstracto_directo '[' ']'
	| declarador_abstracto_directo '[' expresion_constante ']'
	| '(' ')'
	| '(' lista_tipo_parametro ')'
	| declarador_abstracto_directo '(' ')'
	| declarador_abstracto_directo '(' lista_tipo_parametro ')'
	;

inicializador:
	expresion_asignacion
	| '{' lista_inicializador '}'
	| '{' lista_inicializador ',' '}'
	;

lista_inicializador:
	inicializador
	| lista_inicializador ',' inicializador
	;

/* Sentencias. */

sentencia:
	sentencia_etiquetada
	| sentencia_compuesta
	| sentencia_expresion
	| sentencia_seleccion
	| sentencia_iteracion
	| sentencia_salto
	;

sentencia_etiquetada:
	identificador ':' sentencia
	| CASE expresion_constante ':' sentencia
	| DEFAULT ':' sentencia
	;

sentencia_compuesta:
	'{' '}'
	| '{' lista_sentencia '}' 
	| '{' lista_declaracion '}'
	| '{' lista_declaracion lista_sentencia '}'
	;

lista_declaracion:
	declaracion
	| lista_declaracion declaracion
	;

lista_sentencia:
	sentencia
	| lista_sentencia sentencia
	;

sentencia_expresion:
	';'
	| expresion';'
	;

sentencia_seleccion:
	IF '(' expresion')' sentencia
	| IF '(' expresion')' sentencia ELSE sentencia
	| SWITCH '(' expresion')' sentencia
	;

sentencia_iteracion:
	WHILE '(' expresion')' sentencia
	| DO sentencia WHILE '(' expresion')' ';'
	| FOR '(' ';' ';' ')' sentencia
	| FOR '(' expresion';' ';' ')' sentencia
	| FOR '(' ';' expresion';' ')' sentencia
	| FOR '(' expresion';' expresion';' ')' sentencia
	| FOR '(' ';' ';' expresion')' sentencia
	| FOR '(' expresion';' ';' expresion')' sentencia
	| FOR '(' ';' expresion';' expresion')' sentencia
	| FOR '(' expresion';' expresion';' expresion')' sentencia
	;

sentencia_salto:
	GOTO identificador ';'
	| CONTINUE ';'
	| BREAK ';'
	| RETURN ';'
	| RETURN expresion';'
	;

/* Definicion Externa. */

traduccion_unit:
	declaracion_externa
	| traduccion_unit declaracion_externa
	;

declaracion_externa:
	definicion_funcion
	| declaracion
	;

definicion_funcion:
	especificadores_declaracion declarador lista_declaracion sentencia_compuesta
	| especificadores_declaracion declarador sentencia_compuesta
	| declarador lista_declaracion sentencia_compuesta
	| declarador sentencia_compuesta
	;

%%

static void
yyerror(const char *s)
{
	fprintf(stderr, "%d: %s\n", lineno, s);
}

int
main(void)
{
	lineno = 1;
	yyparse();

	return 0;
}