%{
#include "calculadora.tab.h"
%}

%%
[0-9]+("."[0-9]+)?	{	yylval.real = atof(yytext);
				return(REAL);}
"+"|"-"|"*"|"/"|"^"	{	return(yytext[0]);}
"sen"			{	return(SEN);}
"cos"			{	return(COS);}
"("|")"			{	return(yytext[0]);}
"\n"			{	return(yytext[0]);}
.			{}
%%
?