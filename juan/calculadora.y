%{
#include <stdio.h>
#include <math.h>
#include"lex.yy.c"

float	aRadianes (float a){
		return ((a*3.1415926535)/100);
}
%}
%union	{
	float real;
}

%token <real> REAL
%left '-' '+' '*' '/'
%right '^'
%left SEN COS
%type <real> exp

%%
input:	
	|input line
	;
line:	'\n'
	|exp '\n'
	{printf("\t%f\n",$1);}
	;
exp:	REAL {$$ = $1;}
	| exp '+' exp	{$$ = $1 + $3;}
	| exp '-' exp	{$$ = $1 - $3;}
	| exp '*' exp	{$$ = $1 * $3;}
	| exp '/' exp	{
			if($3 == 0){
				printf("No se puede dividir entre cero");}
			else{
				$$ = $1 / $3;
			}
			}
	| exp '^' exp 	{$$ = pow($1,$3);}
	|SEN'('exp')'	{$$ = sin(aRadianes($3));}
	|COS'('exp')'	{$$ = cos(aRadianes($3));}
	|'('exp')'	{$$ = $2;}
%%

int main(){

	yyparse();
	return 1;
}

void yyerror(char *s){
	printf("%s\n",s);
}
int yywrap(){
	return 1;
}

