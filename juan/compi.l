%{
  #include <stdio.h>
  #include <stdlib.h>
  #include "y.tab.h"
  int linea=0;
%}

DIGITO [0-9]
LETRA [a-zA-Z]
IDENTIFICADOR {LETRA}({LETRA}|{DIGITO})*
constEntera {DIGITO}({DIGITO})*
/*SIGNO [< <= > >= != ;]*/
Boleano "True"|"False"|"<"|"<="|">"|">="|"!="

%%

"="         {return(IGUAL);}
"+"         {return(MAS);}
"-"         {return(MENOS);}
","         {return(COMA);}
";"         {return(PTOCOMA);}
"*"         {return(POR);}
"/"         {return(DIV);}
"("         {return(PARENTABIER);}
")"         {return(PARENTCERR);}
"Dim"       {return(DIM);}
"As"        {return (AS);}
"Integer"   {return (TIPO);}
"End"       {return(END);}
"Else"      {return(ELSE);}
"If"    	{return(IF);}
"Then"      {return(THEN);}
"Select"	{return(SELECT);}
"Case"		{return(CASE);}
"While"		{return(WHILE);}
"Do"        {return(DO);}
"Loop"      {return(LOOP);}
"Until"     {return(UNTIL);}
"Next"      {return(NEXT);}
"For"       {return(FOR);}
"Step"      {return(STEP);}
"To"        {return(TO);}
{Boleano}   {return BOLEANO;}
{constEntera} {return (INT);}
{IDENTIFICADOR} {return (ID);}
"\n"        {linea++;}
[\t\r\f] {}
" "                          {}
.            {printf("Error lexico en linea %d",linea);}
%%