%{

  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  extern int yylex(void);
  extern char *yytext;
  extern int linea;
  extern FILE *yyin;
  void yyerror(char *s);
%}

%token DIM AS TIPO COMA MAS MENOS IGUAL PTOCOMA POR DIV PARENTABIER PARENTCERR MAYOR MAYORI MENOR MENORI ID INT END ELSE IF BOLEANO THEN SELECT CASE WHILE
%token DO LOOP UNTIL NEXT FOR STEP TO

%union

{
  float real;
}

%start Expresiones;

%%

/*Expresion general*/
Expresiones: Expresiones Expresion | Expresion | Expresiones Ex | Ex | Expresiones Ex2 | Ex2 | Expresiones Ex3 | Ex3 | Expresiones Ex4 | Ex4 | Expresiones Ex5 | Ex5
                ;
/*gramatica que me reconoce declaracion de variables : "Dim a,b As Integer" */
Expresion  : DIM ListDec
                ;
ListDec    : Dec ListDec | Dec
				;
Dec        : ID ListId
				;
ListId 	   : AS TIPO | COMA ID ListId   
				;
/*gramatica para sentencias If a>b Then sentencia Else sentencia End If */
Ex          : Ife END IF | Ife ELSE ID END IF
				;
Ife 	    : IF Condicion THEN ID
				;
Condicion   : ID BOLEANO ID | ID BOLEANO INT
				;
/* Instrucciones Select...Case */
Ex2			: SELECT CASE ID CaseStatement CaseElseStatement END SELECT
                ;
CaseStatement : Caso | CaseStatement Caso
				;
Caso 		: CASE TipoId ID
				;
TipoId		: ID | INT
				;
CaseElseStatement : CASE ELSE ID
				;
/* Instrucciones While...End While */
Ex3			: WHILE Condi
              ID
              END WHILE
			    ;
Condi  		: ID BOLEANO ID | ID BOLEANO INT
				;
/* Instrucciones Do...Loop */
Ex4			: DO Opcion
			    ;
Opcion  	: Decla ID LOOP | ID LOOP Decla
				;
Decla 		: WhileOrUntil Cond
				;
Cond        : ID BOLEANO ID | ID BOLEANO INT
				;
WhileOrUntil: WHILE | UNTIL
				;
/* For...Next (Instrucciones) */
Ex5			: FOR LoopControlVariable IGUAL INT TO INT STEP INT
              ID
              NEXT
			    ;
LoopControlVariable : ID AS TIPO | ID	
				;
%%

void yyerror(char *s)
{
  printf("Error sintactico %s",s);
}

int main(int argc,char **argv)
{
  if (argc>1)
                yyin=fopen(argv[1],"rt");
  else
                yyin=stdin;

  yyparse();
  system("Pause"); 
  return 0;
}